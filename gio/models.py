from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models

# Create your models here.


class ModelResumes(models.Model):
      owner = models.ForeignKey(User, on_delete=models.CASCADE)
      file = models.FileField(blank=False, null=False)
      status = models.CharField(max_length=20, blank=True, null=True)
      text = models.CharField(max_length=25000, blank=True, null=True)
      timestamp = models.DateTimeField(auto_now_add=True)

      def __unicode__(self):
            return self.id


class ModelCerts(models.Model):
      owner = models.ForeignKey(User, on_delete=models.CASCADE)
      issuer = models.CharField(max_length=100, blank=False)
      cert_url = models.CharField(max_length=1000, blank=False)

      def __unicode__(self):
             return self.id

