from rest_framework import status
from django.contrib.auth.models import User, Group
from rest_framework.decorators import action, api_view, parser_classes
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response
from django.contrib.auth.hashers import make_password
from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from .serializers import ResumeSerializer
from .models import ModelResumes, ModelCerts
from .tasks import process_resume, process_jobs
import threading, json
from django.forms.models import model_to_dict

# Create your views here.


@api_view(['POST'])
# @permission_classes((IsAuthenticated,))
def create_user(request):
    try:
        username = request.data['username']
        password = make_password(request.data['password'])
        email = request.data['email']
        active = True
        created = User(username=username, password=password, email=email, is_active=active).save()
        return Response({"message": "User Created Successfully"}, status=status.HTTP_201_CREATED)
    except:
        return Response({"message": "User Exists / System Issue"}, status=status.HTTP_406_NOT_ACCEPTABLE)


@api_view(['POST'])
@permission_classes((IsAuthenticated, ))
@parser_classes((MultiPartParser, FormParser))
def upload_file(request, *args, **kwargs):
        file_serializer = ResumeSerializer(data=request.data, context={'request': request})
        if file_serializer.is_valid():
            try:
              instance = ModelResumes.objects.get(owner=request.user)
              instance.file = file_serializer.validated_data['file']
              instance.save(update_fields=['file'])
              t = threading.Thread(target=process_resume, args=(instance.file.__str__(), request), kwargs={})
            except:
              file = file_serializer.validated_data['file'].__str__().split()
              file = '_'.join(file)
              ModelResumes(owner=request.user, file=file_serializer.validated_data['file'], status='processing').save()
              t = threading.Thread(target=process_resume, args=(file, request), kwargs={})
            t.setDaemon(True)
            t.start()
            return Response({'message': 'File Uploaded Successfully'}, status=status.HTTP_201_CREATED)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
@permission_classes((IsAuthenticated, ))
def user_details(request):
        finalDict = {}
        instance = ModelResumes.objects.get(owner=request.user)
        dict = model_to_dict(instance, exclude=['file', 'text'])
        dict['file'] = instance.file.__str__()
        finalDict['profile'] = dict
        details = json.loads(instance.text)
        finalDict['details'] = details
        try:
           certInstance = ModelCerts.objects.filter(owner=request.user).values('issuer', 'cert_url')
           finalDict['certs'] = certInstance
        except:
            pass
        return Response(finalDict, status=status.HTTP_200_OK)


@api_view(['PUT'])
@permission_classes((IsAuthenticated, ))
def update_user_details(request):
      try:
            instance = ModelResumes.objects.get(owner=request.user)
            details = json.loads(instance.text)
            details['experience'] = request.data['experience']
            details['primarySkills'] = request.data['primarySkills']
            instance.text = json.dumps(details)
            instance.save(update_fields=['text'])
            t = threading.Thread(target=process_jobs, args=(request, ), kwargs={})
            t.setDaemon(True)
            t.start()
            return Response({"message": "Details Updated"}, status=status.HTTP_200_OK)
      except:
            return Response({"message": "Unsuccessful Attempt"}, status=status.HTTP_400_BAD_REQUEST)





