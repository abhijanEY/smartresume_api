from rest_framework import serializers
from .models import ModelResumes


class ResumeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ModelResumes
        fields = ('file', 'timestamp')

    # def save(self):
    #     print(self.context['request'].user)
    #     print(self.validated_data['file'])
    #     owner = self.context['request'].user
    #     file = self.validated_data['file']



