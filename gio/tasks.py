import pandas as pd
import numpy as np
import PyPDF2
from nltk import sent_tokenize, word_tokenize, pos_tag
from nltk.corpus import stopwords
import string, re, json, math
from .models import ModelResumes, ModelCerts
import pickle


# Background Job to process resume on upload
def process_resume(resume, request):
    all_skills = []
    jobs = {}
    details = {}
    pdfFileObj = open('./media/'+resume, 'rb')
    pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
    pageObj = pdfReader.getPage(0)
    text = pageObj.extractText()
    email = extract_email_addresses(text)
    contact = extract_phone_numbers(text)
    pdfFileObj.close()
    # print(text)
    stop = stopwords.words('english') + list(string.punctuation)
    tokens = word_tokenize(text)
    tokens = [token.lower().strip() for token in tokens if token not in stop]
    sents = sent_tokenize(text)
    dataset = pd.read_csv('jobs.csv')
    for row in dataset.iterrows():
        profile = row[1].values[0]
        skills = [skill.lower().strip() for skill in row[1].values[1].split(',')]
        jobs[profile] = skills
        all_skills = update_all_skills(skills, all_skills)
    candidate_skills = prepare_candidate_skills(tokens, all_skills)
    matched_jobs = prepare_jobs(candidate_skills, jobs)
    final_matched_jobs, relevant_skills = prepare_matched_jobs(matched_jobs, jobs)
    details['email'] = email
    details['contact'] = contact
    details['jobs'] = final_matched_jobs
    details['experience'] = None
    details['primarySkills'] = None
    details['relevantJobs'] = None
    details['relevantSkills'] = list(set(relevant_skills))
    resume_text = json.dumps(details)
    resume = ModelResumes.objects.get(owner=request.user)
    resume.text = resume_text
    resume.status = 'processed'
    resume.save(update_fields=['text', 'status'])
    try:
        ModelCerts.objects.filter(owner=request.user).delete()
    except:
        pass
    certificates = extract_certs(sents)
    for cert in certificates:
        ModelCerts(owner=request.user, issuer=cert['organisation'], cert_url=cert['transactionId']).save()
    # print(dataset)
    # print(all_skills)
    # print(tokens)
    # print(candidate_skills)
    # print(final_matched_jobs)
    # print(matched_jobs)
    # print(relevant_skills)
    # print(email)
    # print(contact)



# Background Job to Run ML match on primary skills and experience update
def process_jobs(request):
    instance = ModelResumes.objects.get(owner=request.user)
    details = json.loads(instance.text)
    exp = details['experience']
    skills = details['primarySkills']
    with open('model.pkl', 'rb') as fin:
        labelencoder, onehotencoder, standard_scaler, classifier = pickle.load(fin)

    input_dict = details['jobs']
    pred_data = []
    for item in input_dict:
        temp = [item['jobTitle'], item['match'], exp, skills[0], skills[1], skills[2]]
        pred_data.append(temp)

    input_feature = np.array(pred_data)

    input_feature[:, 0] = labelencoder.transform(input_feature[:, 0])
    input_feature[:, 3] = labelencoder.transform(input_feature[:, 3])
    input_feature[:, 4] = labelencoder.transform(input_feature[:, 4])
    input_feature[:, 5] = labelencoder.transform(input_feature[:, 5])
    input_feature = onehotencoder.transform(input_feature).toarray()
    input_feature = standard_scaler.transform(input_feature)
    pred = classifier.predict(input_feature)
    print(pred)
    jobs = []
    for i, v in enumerate(pred):
        if v.lstrip().rstrip() == 'true':
            row = pred_data[i]
            jobs.append(row)
    # print(jobs)
    details['relevantJobs'] = jobs
    instance.text = json.dumps(details)
    instance.save(update_fields=['text', ])


# Prepare all skill set
def update_all_skills(skills, allskills):
        for skill in skills:
            if skill not in allskills:
                allskills.append(skill)
        return allskills


# Prepare candidate skills that match relevant skills for job opening
def prepare_candidate_skills(tokens, allskills):
    candidate_skills = []
    for skill in set(allskills):
        regex = skill+'*'
        for token in set(tokens):
            if re.match(regex, token):
                candidate_skills.append(token)
    return candidate_skills


# Prepare Relevant Jobs
def prepare_jobs(candidate_skills, jobs):
    matched_jobs = {}
    for skill in list(set(candidate_skills)):
        for k, v in jobs.items():
            matched_skills = []
            for s in v:
                regex = s+'*'
                if re.match(regex, skill):
                    if k in matched_jobs:
                            matched_skills = matched_jobs.get(k)
                    matched_skills.append(s)
                    matched_jobs[k] = list(set(matched_skills))
    return matched_jobs


# Prepare Json For matched Jobs
def prepare_matched_jobs(matched_jobs, jobs):
    final_matched_jobs = []
    relevant_skills = []
    for job in matched_jobs:
        temp = {}
        skills = matched_jobs.get(job)
        for skill in skills:
            relevant_skills.append(skill)
        total_skills = jobs.get(job)
        percent = math.ceil((len(skills) / len(total_skills)) * 100)
        temp['jobTitle'] = job
        temp['match'] = percent
        temp['candidateSkills'] = skills
        temp['requiredSkills'] = total_skills
        final_matched_jobs.append(temp)
    return final_matched_jobs, relevant_skills


# Sentence Extraction
def extract(s):
    start = s.find('[')
    if start == -1:
        # No opening bracket found. Should this be an error?
        return ''
    start += 1  # skip the bracket, move to the next character
    end = s.find(']', start)
    if end == -1:
        # No closing bracket found after the opening bracket.
        # Should this be an error instead?
        return s[start:]
    else:
        return s[start:end]


# Block Certificate Extraction
def extract_certs(sents):
    transactionIds = []
    certificates = []
    for sent in sents:
        transactionIds.append(extract(sent))
    for id in transactionIds:
        if not id.startswith('Blockcert'):
            transactionIds.remove(id)
    for id in transactionIds:
        data = id.split("/")
        certificate = {}
        certificate['organisation'] = data[1]
        certificate['transactionId'] = data[2]
        certificates.append(certificate)
    return certificates


# Extract Email Addresses
def extract_email_addresses(string):
    r = re.compile(r'[\w\.-]+@[\w\.-]+')
    return r.findall(string)


# Extract Phone Numbers
def extract_phone_numbers(string):
    r = re.compile(r'(\d{3}[-\.\s]??\d{3}[-\.\s]??\d{4}|\(\d{3}\)\s*\d{3}[-\.\s]??\d{4}|\d{3}[-\.\s]??\d{4})')
    phone_numbers = r.findall(string)
    return [re.sub(r'\D', '', number) for number in phone_numbers]