from django.conf.urls import url
from . import views


urlpatterns = [
    # url(r'^', include(router.urls)),
    url(r'^create_user', views.create_user),
    url(r'^upload_file', views.upload_file),
    url(r'^user_details', views.user_details),
    url(r'^update_user_details', views.update_user_details)
]