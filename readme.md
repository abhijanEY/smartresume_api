# Smart Resume API ![CI status](https://img.shields.io/badge/build-passing-brightgreen.svg)

Smart Resume API is built on Python3 and Django REST Framework(DRF) for extending endpoints to Smart Resume UI.

## Features

* JWT Authentication / Authorization.
* Auto resume parsing and skill match with NLP
* Job Match using Machine Learning

## Structure
* /media - Uploaded Files go here.
* /gio - Django/Python Functions
* /Resumes - Sample resumes for trial
* /smartresume - Standard Django structure
* /ML_AI - ML code used for modelling

## Installation

### Requirements
* Linux / Windows
* Python 3.3 and up
* NLTK
* Virtualenv (Optional)

`$ pip install -r requirements.txt`

#### OR

`$ python -m pip install -r requirements.txt` 

####From REPL / Python Shell

```python
import nltk
nltk.download('stopwords')
nltk.download('punkt')
```

## Usage


`$ python manage.py migrate`
`$ python manage.py runserver`

Load the postman collection to see the api endpoints.
Set the globals 'server' & 'jwt' for API hit after server is up.



## Contribution
EY FSOe

