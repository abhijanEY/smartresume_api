# -*- coding: utf-8 -*-
"""
Created on Thu Apr 12 16:28:14 2018

@author: Abhijan.Guha
"""
## Loading Data ##
# importing required modules
import PyPDF2
 
# creating a pdf file object

pdfFileObj = open('../Resumes/Krishnan Jagadesan_Bio.pdf', 'rb')
#pdfFileObj = open('../Resumes/bineesh.pdf', 'rb')
 
# creating a pdf reader object
pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
 
# printing number of pages in pdf file
print(pdfReader.numPages)
 
# creating a page object
pageObj = pdfReader.getPage(0)
 
# extracting text from page
text = pageObj.extractText()

# closing the pdf file object
pdfFileObj.close()

#import nltk
#nltk.download()
from nltk import sent_tokenize, word_tokenize, pos_tag
from nltk.corpus import stopwords
import string
stop = stopwords.words('english') + list(string.punctuation)
sents = sent_tokenize(text)
tokens = word_tokenize(text)
tokens = [token.lower().strip() for token in tokens if token not in stop]


# Importing the dataset
import pandas as pd
dataset = pd.read_csv('jobs.csv')

#Creating Skill listing
allskills = []
jobs = {}

def updateAllSkills(skills):
    for skill in skills:
        if skill not in allskills:
            allskills.append(skill)
            
for row in dataset.iterrows():
    profile = row[1].values[0]
    skills = [skill.lower().strip() for skill in row[1].values[1].split(',')]
    jobs[profile]=skills
    updateAllSkills(skills)
 



#Get Candidate Skills
import re
candidateSkills = []
def getCandidateSkills():
    for skill in set(allskills):
        regex = skill+'*'
        for token in set(tokens):
            if re.match(regex,token):
                candidateSkills.append(token)
getCandidateSkills()      

#Get Relevant Jobs
matchedJobs = {}
def getJobs():
    for skill in candidateSkills:
           for k,v in jobs.items():
                for s in v:
                   regex = s+'*'
                   if re.match(regex,skill):
                       mathchedSkills = []
                       if k in matchedJobs:
                            mathchedSkills = matchedJobs.get(k)
                       mathchedSkills.append(s)
                       matchedJobs[k]=list(set(mathchedSkills))
                       
getJobs()

#Prepare Json For matched Jobs   
finalMatchedJobs = {}                   
def getMatchedJobs():
    for job in matchedJobs:
        temp = {}
        skills = matchedJobs.get(job)
        totalskills = jobs.get(job)
        percent = (len(skills)/len(totalskills))*100
        temp['match']=percent
        temp['candidateSkills']=skills
        temp['requiredSkills']=totalskills
        finalMatchedJobs[job]=temp
            
getMatchedJobs()      

jobdataset = pd.read_csv('mldata.csv')
X = jobdataset.iloc[:, :-1].values
Y = jobdataset.iloc[:, 6].values  
#X_DF = pd.DataFrame(X)

jobdataset.info()
jobdataset.head()

from sklearn.preprocessing import LabelEncoder, OneHotEncoder
onehotencoder = OneHotEncoder(categorical_features=[0])
            








