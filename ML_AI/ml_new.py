# -*- coding: utf-8 -*-
"""
Created on Fri May 11 14:55:45 2018

@author: Abhijan.Guha
"""

import pandas as pd
import numpy as np
import pickle
import json



# Save model and required transformations
def save_model(labelencoder, onehotencoder, standard_scaler, classifier):
    with open('model.pkl', 'wb') as fout:
        pickle.dump((labelencoder, onehotencoder, standard_scaler, classifier), fout)


# Load the saved model and predict
def load_and_predict_model(json,exp,skills):
    exp = 2
    skills =  ['aws','python','s3']
    with open('model.pkl', 'rb') as fin:
        labelencoder, onehotencoder, standard_scaler, classifier = pickle.load(fin)

    input_dict = read_json(input_json)
    pred_data = []
    for item in input_dict:
        temp = [item['jobTitle'],item['match'],exp,skills[0],skills[1],skills[2]]
        pred_data.append(temp)

    input_feature = np.array(pred_data)

    input_feature[:, 0] = labelencoder.transform(input_feature[:, 0])
    input_feature[:, 3] = labelencoder.transform(input_feature[:, 3])
    input_feature[:, 4] = labelencoder.transform(input_feature[:, 4])
    input_feature[:, 5] = labelencoder.transform(input_feature[:, 5])
    input_feature = onehotencoder.transform(input_feature).toarray()
    input_feature = standard_scaler.transform(input_feature)
    pred = classifier.predict(input_feature)
    print(pred)
    
    jobs = []
    for i,v in enumerate(pred):
        if v == 1:
           row = pred_data[i]
           jobs.append(row)
    print(jobs)       
            


def read_json(json_data):
    return json.loads(json_data)

def normalize(data):
    data = [item.lower().lstrip().rstrip() for item in data]
    return data


# ----------------------- Model Creation-------------------------------
# Reading Data
#mldata = pd.read_csv('mldata.csv')
mldata = pd.read_csv('ModelCandidatesData.csv')

X = mldata.iloc[:, :-1].values
y = mldata.iloc[:, -1].values
roles = list(set(mldata.iloc[:, 0].values.tolist()))
skills = normalize(list(set(mldata.iloc[:, 3].values.tolist()+mldata.iloc[:, 4].values.tolist()+mldata.iloc[:, 5].values.tolist())))



# Encoding Categorical Data
from sklearn.preprocessing import LabelEncoder, OneHotEncoder

labelencoder_X = LabelEncoder()
labelencoder_X.fit(skills+roles)

X[:, 0] = labelencoder_X.transform(X[:, 0])
X[:, 3] = labelencoder_X.transform(normalize(X[:, 3]))
X[:, 4] = labelencoder_X.transform(normalize(X[:, 4]))
X[:, 5] = labelencoder_X.transform(normalize(X[:, 5]))



onehotencoder = OneHotEncoder(categorical_features=[0,3,4,5])
X = onehotencoder.fit_transform(X).toarray()

# print(X)

# Splitting the dataset into the Training set and Test set
from sklearn.cross_validation import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.10, random_state=0)

# Feature Scaling
from sklearn.preprocessing import StandardScaler

sc = StandardScaler()
X_train = sc.fit_transform(X_train)

# print(len(X_train))

X_test = sc.transform(X_test)

# Fitting Logistic Regression to the Training set
from sklearn.linear_model import LogisticRegression

classifier = LogisticRegression(random_state=0)
classifier.fit(X_train, y_train)

# Predicting the Test set results
y_pred = classifier.predict(X_test)


# Save model
save_model(labelencoder_X, onehotencoder, sc, classifier)

# input json format
input_json = json.dumps(jobs)

# Load saved model and predict
load_and_predict_model(input_json,2)

jobs = [
            {
                "jobTitle": "Full Stack Engineer",
                "match": 20,
                "candidateSkills": [
                    "java"
                ],
                "requiredSkills": [
                    "java",
                    "microservice",
                    "angular 4",
                    "spring boot",
                    "bootstrap"
                ]
            },
            {
                "jobTitle": "Devops",
                "match": 29,
                "candidateSkills": [
                    "jenkins",
                    "python"
                ],
                "requiredSkills": [
                    "jenkins",
                    "docker",
                    "kubernetes",
                    "python",
                    "perl",
                    "shell scripting",
                    "ansible"
                ]
            },
            {
                "jobTitle": "Cloud Architect",
                "match": 40,
                "candidateSkills": [
                    "aws",
                    "s3"
                ],
                "requiredSkills": [
                    "aws",
                    "azure",
                    "redshift",
                    "s3",
                    "dynamodb"
                ]
            },
            {
                "jobTitle": "Python Engineer",
                "match": 40,
                "candidateSkills": [
                    "aws",
                    "python"
                ],
                "requiredSkills": [
                    "python",
                    "pip",
                    "flask",
                    "django",
                    "aws"
                ]
            },
            {
                "jobTitle": "Integration Engineer",
                "match": 25,
                "candidateSkills": [
                    "tibco"
                ],
                "requiredSkills": [
                    "spring boot",
                    "mulesoft",
                    "tibco",
                    "api gateway"
                ]
            }
        ]